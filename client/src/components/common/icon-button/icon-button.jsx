import PropTypes from 'prop-types';

import { IconName } from 'common/enums/enums.js';
import { Icon } from 'components/common/common.js';

import styles from './styles.module.scss';

const IconButton = ({
  iconName,
  label,
  onClick,
  attributes
}) => (
  <button className={styles.iconButton} type="button" onClick={onClick} {...attributes}>
    <Icon name={iconName} />
    {label}
  </button>
);

IconButton.propTypes = {
  iconName: PropTypes.oneOf(Object.values(IconName)).isRequired,
  label: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onClick: PropTypes.func.isRequired,
  attributes: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.bool, PropTypes.string]))
};

IconButton.defaultProps = {
  label: '',
  attributes: {}
};

export { IconButton };
