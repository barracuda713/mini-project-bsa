import PropTypes from 'prop-types';
import io from 'socket.io-client';
import {
  NotificationContainer,
  NotificationManager
} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import { useEffect, useCallback, useDispatch } from 'hooks/hooks';
import { ENV, NotificationMessage, SocketEvent } from 'common/enums/enums';
import { userType, errorType } from 'common/prop-types/prop-types';
import { commonActionCreator } from 'store/actions';

const socket = io(ENV.SOCKET_URL);

const Notifications = ({ user, onPostApply, error }) => {
  const dispatch = useDispatch();
  const handleClearError = useCallback(() => (
    dispatch(commonActionCreator.clearError())
  ), [dispatch]);

  useEffect(() => {
    if (Object.keys(error).length !== 0) {
      NotificationManager.error(error.message);
      setTimeout(handleClearError, 3000);
    }
    if (!user) {
      return undefined;
    }
    const { id } = user;
    socket.emit(SocketEvent.CREATE_ROOM, id);
    socket.on(SocketEvent.LIKE, () => {
      NotificationManager.info(NotificationMessage.LIKED_POST);
    });
    socket.on(SocketEvent.NEW_POST, post => {
      if (post.userId !== id) {
        onPostApply(post.id);
      }
    });

    return () => {
      socket.emit(SocketEvent.LEAVE_ROOM, id);
      socket.removeAllListeners();
    };
  }, [user, onPostApply, error, handleClearError]);

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined,
  error: {}
};

Notifications.propTypes = {
  user: userType,
  error: errorType,
  onPostApply: PropTypes.func.isRequired
};

export { Notifications };
