import {
  faAt,
  faBookmark,
  faCircleNotch,
  faComment,
  faCopy,
  faFrown,
  faImage,
  faLock,
  faPen,
  faShareAlt,
  faSignOutAlt,
  faThumbsDown,
  faThumbtack,
  faThumbsUp,
  faTrashCan,
  faUser,
  faUserCircle,
  faEllipsis
} from '@fortawesome/free-solid-svg-icons';
import { IconName } from 'common/enums/enums';

const iconNameToSvgIcon = {
  [IconName.AT]: faAt,
  [IconName.COMMENT]: faComment,
  [IconName.COPY]: faCopy,
  [IconName.DELETE]: faTrashCan,
  [IconName.EDIT]: faPen,
  [IconName.FROWN]: faFrown,
  [IconName.IMAGE]: faImage,
  [IconName.LOCK]: faLock,
  [IconName.LOG_OUT]: faSignOutAlt,
  [IconName.MENU_OPTIONS]: faEllipsis,
  [IconName.PIN]: faThumbtack,
  [IconName.SAVE]: faBookmark,
  [IconName.SHARE_ALTERNATE]: faShareAlt,
  [IconName.SPINNER]: faCircleNotch,
  [IconName.THUMBS_UP]: faThumbsUp,
  [IconName.THUMBS_DOWN]: faThumbsDown,
  [IconName.USER]: faUser,
  [IconName.USER_CIRCLE]: faUserCircle
};

export { iconNameToSvgIcon };
