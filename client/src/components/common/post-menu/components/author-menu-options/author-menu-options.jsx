import PropTypes from 'prop-types';
import { useCallback, useDispatch } from 'hooks/hooks.js';
import { threadActionCreator } from 'store/actions.js';
import { IconName } from 'common/enums/enums';
import { Icon } from 'components/common/common';

const AuthorMenuOptions = ({ styles, onEditPostToggle, onDeletePost }) => {
  const dispatch = useDispatch();
  const handleExpandedPostToggle = useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handleEditPostToggle = () => {
    handleExpandedPostToggle();
    onEditPostToggle();
  };

  const handleDeletePost = () => onDeletePost();

  return (
    <>
      <li>
        <div className={styles} onClick={handleEditPostToggle}>
          <Icon
            name={IconName.EDIT}
          />
          <span>Edit post</span>
        </div>
      </li>
      <li>
        <div className={styles} onClick={handleDeletePost}>
          <Icon
            name={IconName.DELETE}
          />
          <span>Delete post</span>
        </div>
      </li>
    </>
  );
};

AuthorMenuOptions.defaultProps = {
  styles: ''
};

AuthorMenuOptions.propTypes = {
  styles: PropTypes.string,
  onEditPostToggle: PropTypes.func.isRequired,
  onDeletePost: PropTypes.func.isRequired
};

export { AuthorMenuOptions };
