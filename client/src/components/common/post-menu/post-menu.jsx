import PropTypes from 'prop-types';
import { useEffect, useCallback, useDispatch } from 'hooks/hooks.js';
import { threadActionCreator } from 'store/actions.js';
import { AuthorMenuOptions, Divider } from './components/components.js';

import styles from './styles.module.scss';

const PostMenu = ({ isAuthor, postId, isOpen, postMenuToggleId, onOutsideClick, onEditPostToggle }) => {
  const dispatch = useDispatch();
  const handleDeletePost = useCallback(() => (
    dispatch(threadActionCreator.deletePost(postId))
  ), [dispatch, postId]);
  const handleEditPostToggle = () => onEditPostToggle(postId);

  const menuListener = useCallback(
    e => {
      const toggle = document.getElementById(postMenuToggleId);
      const menu = document.getElementById('postMenu');
      if (((e.target !== menu) && (!menu.contains(e.target)))
        && ((e.target !== toggle) && (!toggle.contains(e.target)))) {
        onOutsideClick();
      }
    },
    [postMenuToggleId, onOutsideClick]
  );

  useEffect(() => {
    window.addEventListener('mouseup', menuListener);
    return () => {
      window.removeEventListener('mouseup', menuListener);
    };
  }, [menuListener]);

  return (
    <div className={styles.cardMenu} id="postMenu" aria-hidden={isOpen} aria-labelledby={postMenuToggleId}>
      <div>
        <ul className={styles.cardMenu__list}>
          { isAuthor
            && (
              <>
                <AuthorMenuOptions
                  styles={styles.cardMenu__item}
                  onEditPostToggle={handleEditPostToggle}
                  onDeletePost={handleDeletePost}
                />
                <Divider />
              </>
            )}
        </ul>
      </div>
    </div>
  );
};

PostMenu.propTypes = {
  isAuthor: PropTypes.bool.isRequired,
  postId: PropTypes.number.isRequired,
  isOpen: PropTypes.bool.isRequired,
  postMenuToggleId: PropTypes.string.isRequired,
  onOutsideClick: PropTypes.func.isRequired,
  onEditPostToggle: PropTypes.func.isRequired
};

export { PostMenu };
