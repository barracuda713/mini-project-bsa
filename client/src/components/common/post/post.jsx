import PropTypes from 'prop-types';
import { useState } from 'hooks/hooks.js';

import { PostMenu } from 'components/common/common.js';

import { getFromNowTime } from 'helpers/helpers';
import { IconName } from 'common/enums/enums';
import { postType } from 'common/prop-types/prop-types';
import { IconButton, Image } from 'components/common/common';

import styles from './styles.module.scss';

const Post = ({ post, onPostLike, onPostDislike, onExpandedPostToggle, onEditPostToggle, onSharePost, userId }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);

  const [postMenuOpen, setPostMenuOpen] = useState(false);
  const handlePostMenuOpen = e => {
    e.stopPropagation();
    setPostMenuOpen(!postMenuOpen);
  };
  const handlePostMenuClose = () => setPostMenuOpen(false);

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handleEditPostToggle = () => onEditPostToggle(id);
  const handleSharePost = () => onSharePost(id);

  return (
    <div className={styles.card}>
      {image && <Image src={image.link} alt="post image" />}
      <div className={styles.content}>
        <div className={styles.meta}>
          <span>{`posted by ${user.username} - ${date}`}</span>
          <div className={styles.menu}>
            <IconButton
              iconName={IconName.MENU_OPTIONS}
              onClick={handlePostMenuOpen}
              attributes={{
                'aria-expanded': postMenuOpen,
                'aria-controls': `postMenuToggle-${id}`,
                id: `postMenuToggle-${id}`
              }}
            />
            {postMenuOpen
              && (
                <PostMenu
                  isAuthor={userId === user.id}
                  postId={id}
                  isOpen={postMenuOpen}
                  postMenuToggleId={`postMenuToggle-${id}`}
                  onOutsideClick={handlePostMenuClose}
                  onEditPostToggle={handleEditPostToggle}
                />
              )}
          </div>
        </div>
        <p className={styles.description}>{body}</p>
      </div>
      <div className={styles.extra}>
        <IconButton
          iconName={IconName.THUMBS_UP}
          label={likeCount}
          onClick={handlePostLike}
        />
        <IconButton
          iconName={IconName.THUMBS_DOWN}
          label={dislikeCount}
          onClick={handlePostDislike}
        />
        <IconButton
          iconName={IconName.COMMENT}
          label={commentCount}
          onClick={handleExpandedPostToggle}
        />
        <IconButton
          iconName={IconName.SHARE_ALTERNATE}
          onClick={handleSharePost}
        />
      </div>
    </div>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  onEditPostToggle: PropTypes.func.isRequired,
  onSharePost: PropTypes.func.isRequired,
  userId: PropTypes.number.isRequired
};

export { Post };
