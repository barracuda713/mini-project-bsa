import { useCallback, useDispatch, useSelector } from 'hooks/hooks.js';
import { threadActionCreator } from 'store/actions.js';
import { image as imageService } from 'services/services.js';
import { Spinner, Modal } from 'components/common/common.js';
import { UpdatePost } from '../update-post/update-post.jsx';

const EditPost = () => {
  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.editPost
  }));

  const handlePostUpdate = useCallback(
    postPayload => dispatch(threadActionCreator.updatePost(postPayload)),
    [dispatch]
  );

  const handleUploadImage = file => imageService.uploadImage(file);

  const handleEditPostToggle = useCallback(id => (
    dispatch(threadActionCreator.toggleEditPost(id))
  ), [dispatch]);

  const handleEditPostClose = () => handleEditPostToggle();

  return (
    <Modal
      isOpen
      onClose={handleEditPostClose}
    >
      {post ? (
        <UpdatePost onPostUpdate={handlePostUpdate} onUploadImage={handleUploadImage} onClose={handleEditPostClose} />
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

EditPost.propTypes = {};

export { EditPost };
