import PropTypes from 'prop-types';
import { useSelector, useCallback, useState, useEffect, useAppForm, useDispatch } from 'hooks/hooks.js';
import { commonActionCreator } from 'store/actions';
import { ButtonColor, ButtonType, IconName, PostPayloadKey } from 'common/enums/enums.js';
import { Button, Image, Input, Segment } from 'components/common/common.js';
import { DEFAULT_ADD_POST_PAYLOAD } from './common/constants.js';

import styles from './styles.module.scss';

const UpdatePost = ({ onPostUpdate, onUploadImage, onClose }) => {
  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.editPost
  }));

  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);

  const { control, handleSubmit, reset, setValue } = useAppForm({
    defaultValues: DEFAULT_ADD_POST_PAYLOAD
  });

  const handleGetError = useCallback(err => (
    dispatch(commonActionCreator.getError(err))
  ), [dispatch]);

  const handleUpdatePost = useCallback(
    values => {
      if (!values.body) {
        return;
      }
      onPostUpdate({
        id: post.id,
        post: {
          imageId: image?.imageId,
          body: values.body
        }
      }).then(() => {
        reset();
        setImage(undefined);
        onClose();
      });
    },
    [image, reset, onPostUpdate, post.id, onClose]
  );

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    onUploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
      })
      .catch(e => {
        handleGetError(e);
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  useEffect(() => {
    setImage({
      imageId: post.image?.id,
      imageLink: post.image?.link
    });
    setValue(PostPayloadKey.BODY, post.body);
  }, [setImage, setValue, post]);

  return (
    <Segment>
      <form onSubmit={handleSubmit(handleUpdatePost)}>
        <Input
          name={PostPayloadKey.BODY}
          placeholder="What is the news?"
          rows={5}
          control={control}
        />
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post image" />
          </div>
        )}
        <div className={styles.btnWrapper}>
          <Button
            color="teal"
            isLoading={isUploading}
            isDisabled={isUploading}
            iconName={IconName.IMAGE}
          >
            <label className={styles.btnImgLabel}>
              Attach image
              <input
                name="image"
                type="file"
                onChange={handleUploadFile}
                hidden
              />
            </label>
          </Button>
          <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
            Update post
          </Button>
        </div>
      </form>
    </Segment>
  );
};

UpdatePost.propTypes = {
  onPostUpdate: PropTypes.func.isRequired,
  onUploadImage: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export { UpdatePost };
