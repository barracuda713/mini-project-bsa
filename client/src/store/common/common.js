const ActionType = {
  GET_ERROR: 'common/get-error',
  CLEAR_ERROR: 'common/clear-error'
};

export { ActionType };
