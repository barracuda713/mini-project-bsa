import { createAction } from '@reduxjs/toolkit';

import { ActionType } from './common.js';

const getError = createAction(ActionType.GET_ERROR);
const clearError = createAction(ActionType.CLEAR_ERROR);

export { getError, clearError };
