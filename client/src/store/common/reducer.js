import { createReducer } from '@reduxjs/toolkit';
import {
  getError,
  clearError
} from './actions.js';

const initialState = {
  error: {}
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(getError, (state, action) => {
    state.error = action.payload;
  });
  builder.addCase(clearError, state => {
    state.error = {};
  });
});

export { reducer };
