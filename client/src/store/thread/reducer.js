import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import {
  loadPosts,
  loadMorePosts,
  toggleExpandedPost,
  toggleEditPost,
  likePost,
  addComment,
  applyPost,
  createPost,
  updatePost,
  deletePost
} from './actions.js';

const initialState = {
  posts: [],
  expandedPost: null,
  editPost: null,
  hasMorePosts: true
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(loadPosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(loadMorePosts.pending, state => {
    state.hasMorePosts = null;
  });
  builder.addCase(loadMorePosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(toggleExpandedPost.fulfilled, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
  builder.addCase(toggleEditPost.fulfilled, (state, action) => {
    const { post } = action.payload;

    state.editPost = post;
  });
  builder.addCase(updatePost.fulfilled, (state, action) => {
    const { post } = action.payload;

    state.posts = state.posts.map(p => {
      return (p.id === post.id) ? post : p;
    });
  });
  builder.addCase(deletePost.fulfilled, (state, action) => {
    const { arg: postId } = action.meta;
    const posts = [...state.posts];
    const index = state.posts.findIndex(p => p.id === postId);
    if (index !== -1) posts.splice(index, 1);

    state.posts = [...posts];
  });
  builder.addMatcher(
    isAnyOf(likePost.fulfilled, addComment.fulfilled),
    (state, action) => {
      const { posts, expandedPost } = action.payload;
      state.posts = posts;
      state.expandedPost = expandedPost;
    }
  );
  builder.addMatcher(
    isAnyOf(applyPost.fulfilled, createPost.fulfilled),
    (state, action) => {
      const { post } = action.payload;

      state.posts = [post, ...state.posts];
    }
  );
});

export { reducer };
