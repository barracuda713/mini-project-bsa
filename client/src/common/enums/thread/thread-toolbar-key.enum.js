const ThreadToolbarKey = {
  SHOW_OWN_POSTS: 'showOwnPosts',
  HIDE_OWN_POSTS: 'hideOwnPosts'
};

export { ThreadToolbarKey };
