import PropTypes from 'prop-types';

const errorType = PropTypes.exact({
  name: PropTypes.string,
  message: PropTypes.string,
  stack: PropTypes.string
});

export { errorType };
